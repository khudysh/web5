function click1() {
    let f1 = document.getElementsByName("field1");
    let f2 = document.getElementsByName("field2");
    let r = document.getElementById("result");
    let tmpRes = f1[0].value * f2[0].value;
    if (Number.isNaN(tmpRes)) {
        r.placeholder = "Enter numbers please!";
    } else {
        r.placeholder = tmpRes;
    }
    f1 = document.getElementsByName("field1").value = "";
    f2 = document.getElementsByName("field2").value = "";
    return false;
}